import 'package:flutter/material.dart';

// ignore: camel_case_types
class listPage extends StatelessWidget {
  const listPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Daftar Penyakit"),
      ),
      body: Center(
        child: SafeArea(
          child: Column(
            children: [
              ExpansionTile(
                title: Text("Gingivitis (Radang Gusi)"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Gingivitis (radang gusi) adalah penyakit akibat infeksi bakteri yang menyebabkan gusi bengkak karena meradang. '
                    'Penyebab utama kondisi ini adalah kebersihan mulut yang buruk. Orang yang jarang sikat gigi, sering makan makanan yang manis dan asam, '
                    'tidak rutin cek gigi ke dokter adalah yang paling berisiko mengalami gingivitis.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Karies Gigi (Gigi Berlubang)"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Gigi berlubang atau karies gigi merupakan kondisi kerusakan pada bagian terluar (enamel) dan terdalam (dentin) gigi. '
                    'Dalam istilah kedokteran, gigi berlubang disebut dengan cavities atau karies. Pada dasarnya, karies gigi adalah proses '
                    'pembusukan yang mengakibatkan kerusakan pada email gigi, dentin, bahkan sampai sementum gigi. Munculnya lubang pada gigi bisa disebabkan banyak faktor.'
                    'Mulai dari bakteri di dalam mulut, mengonsumsi makanan dan minuman manis, hingga kebersihan mulut yang buruk.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Karang Gigi"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Karang gigi adalah plak yang mengendap dan mengeras di permukaan gigi. Dalam istilah medis, '
                    'masalah gigi ini disebut dental calculus. Plak adalah lapisan tipis dan lengket yang terbuat dari kumpulan bakteri, kotoran, dan sisa-sisa makanan. '
                    'Plak membutuhkan waktu sekitar 12 hari untuk matang dan mengeras hingga menjadi karang. Namun, kecepatan pembentukan karang '
                    'pada setiap orang sebenarnya berbeda tergantung pada kadar pH air liur.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Stomatitis"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Stomatitis adalah peradangan berupa bengkak atau kemerahan yang umumnya dapat ditemukan pada bagian mulut. '
                    'Peradangan dapat muncul di pipi, gusi, bagian dalam bibir, atau lidah. Penyakit ini biasanya memengaruhi selaput halus yang melapisi mulut '
                    'dan memproduksi lendir (mukosa). Lendir ini berguna untuk melindungi sistem pencernaan tubuh, mulai dari mulut hingga anus. '
                    'Stomatitis adalah salah satu jenis mukositis, suatu kondisi di mana peradangan terjadi pada selaput mukosa. '
                    'Mukositis umumnya merupakan efek samping dari kemoterapi atau radioterapi.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Candidas Oral"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Kandidiasis oral adalah infeksi fungal yang mengenai mukosa oral, disebabkan oleh Candida sp.'
                    'Candida albicans adalah spesies Candida yang paling banyak ditemukan di kavitas oral individu sehat maupun yang mengalami kandidiasis.'
                    'Penyakit ini paling sering mengenai pasien dengan gangguan sistem imun, misalnya pasien diabetes mellitus, menjalani kemoterapi, atau mengonsumsi kortikosteroid.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Trench Mouth (Infeksi Gigi)"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Trench mouth adalah penyakit infeksi gusi yang dapat berkembang dengan cepat.'
                    'Penyakit ini merupakan jenis dari gingivitis parah yang menyebabkan rasa sakit, infeksi, dan perdarahan pada gusi.'
                    'Sebutan trench mouth atau “mulut parit” diambil dari istilah yang banyak digunakan selama Perang Dunia I,'
                    'di mana saat itu banyak tentara di parit medan perang yang menderita infeksi pada gusi.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Gigi Sensitif"),
                leading: Icon(Icons.local_hospital),
                children: [
                  Text(
                    'Gigi sensitif adalah kondisi ketika gigi terasa nyeri dan ngilu akibat lapisan dalamnya yang disebut dentin terekspos ke lingkungan luar.'
                    'Rasa nyeri mungkin terasa sampai ke gusi. Dentin sendiri terhubung dengan saluran yang dipenuhi oleh serabut saraf.'
                    'Paparan suhu dingin dan panas, bahkan senyawa asam pada dentin dapat juga mengenai serabut saraf tersebut.'
                    'Akibatnya, gigi Anda akan terasa ngilu, cenat-cenut, dan tidak nyaman.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
